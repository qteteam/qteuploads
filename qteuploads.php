<?php
/*
 * Plugin Name: QTEUploads
 * Description: QTE Uploads fixes common wordpress upload problems
 * Version: 0.1.0
 * Author: QTE Development AB
 * Author URI: https://getqte.se/
 */

 if( ! defined('ABSPATH')) {
     exit;
 }

 define('QTEUPLOADS_VERSION', '0.1.0');

define( 'QTEUPLOADS_PLUGIN_DIR', plugin_dir_url( __FILE__ ) );
define( 'QTEUPLOADS_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );


require_once QTEUPLOADS_PLUGIN_PATH . 'admin/uploads.php';
require_once QTEUPLOADS_PLUGIN_PATH . 'admin/svg.php';
