<?php

namespace QTEUPLOADS\SVG;

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

// Allow upload SVG
function add_svg_cc_mime($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('mime_types', __NAMESPACE__ . '\\add_svg_cc_mime', 50);